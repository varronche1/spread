CREATE TABLE IF NOT EXISTS `person` (
  `person_id` decimal(5) NOT NULL,
  `first_name` varchar(50) NULL,
  `last_name` varchar(50) NULL,
  `linkedin_url` varchar(150) NULL,
  `whatsapp` varchar(11) NULL,
  `mail` varchar(50) NULL,
  PRIMARY KEY (`person_id`)
);
INSERT INTO `person` (`person_id`, `first_name`, `last_name`, `linkedin_url`, `whatsapp`, `mail`) VALUES
	(1, 'Person1', 'Person1', 'linkein1.com.br', '11999999999', 'person1@mail.com.br'),
	(2, 'Person2', 'Person2', 'linkein2.com.br', '11988888888', 'person2@mail.com.br'),
	(3, 'Person3', 'Person3', 'linkein3.com.br', '11977777777', 'person3@mail.com.br'),
	(4, 'Person4', 'Person4', 'linkein4.com.br', '11966666666', 'person4@mail.com.br'); 

CREATE TABLE IF NOT EXISTS `skill` (
  `skill_id` decimal(5) NOT NULL,
  `skill_tag` varchar(50) NULL,
  `skill_description` varchar(50) NULL,
  PRIMARY KEY (`skill_id`)
);
INSERT INTO `skill` (`skill_id`, `skill_tag`, `skill_description`) VALUES
	(1, 'Tag1', 'Description1'),
	(2, 'Tag2', 'Description2'),
	(3, 'Tag3', 'Description3'),
	(4, 'Tag4', 'Description4'); 

CREATE TABLE IF NOT EXISTS `person_skill` (
  `person_id` decimal(5) unsigned NOT NULL,
  `skill_id` decimal(5) unsigned NOT NULL,
  PRIMARY KEY (`person_id`,`skill_id`)
);

INSERT INTO `person_skill` (`person_id`, `skill_id`) VALUES
	(1,1),
	(1,2),
	(1,4),
	(2,2),
	(3,3),
	(4,4); 