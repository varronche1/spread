package br.com.spread.resources;

import java.net.URI;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import br.com.spread.models.Skill;
import br.com.spread.services.ISkillService;

/**
 * Class that represents the rest services to a skill entity
 * 
 * @author VANESSA
 *
 */
@Path("/")
@Transactional
public class SkillResource {
	
	private static final Logger logger = LoggerFactory.getLogger(SkillResource.class);	

	@Autowired
	private ISkillService skillService;

	@Path("/addSkill")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addSkill(Skill skill) {

		boolean isAdded = skillService.addSkill(skill);
		if (!isAdded) {
			 logger.info("Skill already exits.");
			return Response.status(Status.CONFLICT).build();
		}
		return Response.created(URI.create("/spread/skill/"+ skill.getSkillId())).build();
	}	
	
	@Path("/deleteSkill/{skillId}")
	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)		
	public Response deleteSkill(@PathParam("skillId") Long skillId) {
		
		skillService.deleteSkill(skillId);
		return Response.noContent().build();
	}

	@Path("/allSkills")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllSkills() {

		final List<Skill> skill = skillService.getAllSkills();

		return Response.ok(skill).build();
	}

	@Path("/skill/{skillId}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSkillById(@PathParam("skillId") final Long skillId) {

		final Skill skill = skillService.getSkillById(skillId);

		return Response.ok(skill).build();
	}

}
