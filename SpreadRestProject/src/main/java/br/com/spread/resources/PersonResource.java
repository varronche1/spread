package br.com.spread.resources;

import java.net.URI;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import br.com.spread.models.Person;
import br.com.spread.services.IPersonService;

/**
 * Class that represents the rest services to a person entity
 * 
 * @author VANESSA
 *
 */
@Path("/")
@Transactional
public class PersonResource {
	
	private static final Logger logger = LoggerFactory.getLogger(PersonResource.class);	

	@Autowired
	private IPersonService personService;

	@Path("/addPerson")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addPerson(Person person) {

		boolean isAdded = personService.addPerson(person);
		if (!isAdded) {
			 logger.info("Person already exits.");
			return Response.status(Status.CONFLICT).build();
		}
		return Response.created(URI.create("/spread/person/"+ person.getPersonId())).build();
	}	
	
	@Path("/deletePerson/{personId}")
	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)		
	public Response deletePerson(@PathParam("personId") Long personId) {
		
		personService.deletePerson(personId);
		return Response.noContent().build();
	}

	@Path("/justPeople")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getJustPeople() {

		final List<Person> people = personService.getJustPeople();

		return Response.ok(people).build();
	}

	@Path("/peopleSkill")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPeopleAndSkills() {

		final List<Person> people = personService.getPeopleAndSkills();

		return Response.ok(people).build();
	}

	@Path("/person/{personId}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPersonById(@PathParam("personId") final Long personId) {

		final Person person = personService.getPersonById(personId);

		return Response.ok(person).build();
	}

}
