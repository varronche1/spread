package br.com.spread.utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import br.com.spread.models.Person;
import br.com.spread.models.Skill;

public class Utils {

	public static List<Person> getPersonList(Iterable<Person> iterable, boolean withoutSkill) {

		List<Person> peopleList = new ArrayList<Person>();
		Set<Skill> skills = new HashSet<Skill>();

		Iterator<Person> it = iterable.iterator();

		while(it.hasNext()) {
			Person p = it.next();

			if (withoutSkill)
				p.setSkills(skills);

			peopleList.add(p);
		}

		return peopleList;
	}

	public static List<Skill> getSkillList(Iterable<Skill> iterable) {

		List<Skill> skillList = new ArrayList<Skill>();

		Iterator<Skill> it = iterable.iterator();

		while(it.hasNext()) 
			skillList.add(it.next());

		return skillList;
	}
}
