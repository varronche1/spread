package br.com.spread.services;

import java.util.List;

import br.com.spread.models.Person;

public interface IPersonService {
	
	/**
	 * Get all persons excluding relationships (skill)
	 * @return
	 */
	public List<Person> getJustPeople();
	
	/**
	 * Get all persons including relationships (skill)
	 * @return
	 */
	public List<Person> getPeopleAndSkills();
	
	/**
	 * Get an specific person including relationships using person_id as parameter / 
	 * Get set of skill for specific person_id
	 * @param personId
	 * @return
	 */
	public Person getPersonById(Long personId);

	/**
	 * Add an entity (person)
	 * @param person
	 * @return
	 */
	public Boolean addPerson(Person person);
	
	/**
	 * Delete an entity (person)
	 * @param personId
	 */
	public void deletePerson(Long personId);

}
