package br.com.spread.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.spread.models.Skill;
import br.com.spread.repository.SkillRepository;
import br.com.spread.utils.Utils;

@Service
public class SkillService implements ISkillService{
	
	
		@Autowired
		private SkillRepository repository;

		@Override
		public List<Skill> getAllSkills() {
			
			Iterable<Skill> skills = repository.findAll();
			
			return Utils.getSkillList(skills);
		}
		
		@Override
		public Skill getSkillById(Long skillId) {
			
			Skill s = repository.findOne(skillId);
			
			return s; 
		}

		@Override
		public Boolean addSkill(Skill skill) {
			
			Skill s = repository.save(skill);
			
			if (s != null)
				return true;
			
			return false;
		}

		@Override
		public void deleteSkill(Long skillId) {

			repository.delete(skillId);
		}

}
