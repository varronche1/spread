package br.com.spread.services;

import java.util.List;

import br.com.spread.models.Skill;

public interface ISkillService {
	
	/**
	 * Get all skills
	 * @return
	 */
	public List<Skill> getAllSkills();
	
	/**
	 * Get an specific skill using skill_id as parameter 
	 * @param skillId
	 * @return
	 */
	public Skill getSkillById(Long skillId);

	/**
	 * Add an entity (skill)
	 * @param skill
	 * @return
	 */
	public Boolean addSkill(Skill skill);
	
	/**
	 * Delete an entity (skill)
	 * @param skillId
	 */
	public void deleteSkill(Long skillId);

}
