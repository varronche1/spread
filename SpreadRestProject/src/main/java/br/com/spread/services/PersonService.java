package br.com.spread.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.spread.models.Person;
import br.com.spread.repository.PersonRepository;
import br.com.spread.utils.Utils;

@Service
public class PersonService implements IPersonService{
	
	@Autowired
	private PersonRepository repository;

	@Override
	public List<Person> getJustPeople() {
		
		Iterable<Person> people = repository.findAll();
		
		return Utils.getPersonList(people, true);
	}
	
	@Override
	public List<Person> getPeopleAndSkills() {

		Iterable<Person> people = repository.findAll();

		return Utils.getPersonList(people, false);
	}
	
	@Override
	public Person getPersonById(Long personId) {
		
		Person p = repository.findOne(personId);
		
		return p; 
	}

	@Override
	public Boolean addPerson(Person person) {
		
		Person p = repository.save(person);
		
		if (p != null)
			return true;
		
		return false;
	}

	@Override
	public void deletePerson(Long personId) {

		repository.delete(personId);
	}
}
