package br.com.spread;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

@Component
@ApplicationPath("/spread")
public class JerseyConfig extends ResourceConfig{
	public JerseyConfig() {
		packages("br.com.spread.resources");
	}
}
