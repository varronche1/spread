package br.com.spread.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.spread.models.Person;

/**
 * CRUD - person entity
 * @author VANESSA
 *
 */
@Repository
public interface PersonRepository extends JpaRepository<Person, Long>{

}
