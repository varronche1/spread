package br.com.spread.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;

/**
 * Class represents a personl
 * Table = person
 * @author VANESSA
 *
 */
@Entity
@NamedQuery(query = "select p from Person p", name = "query_find_all_people")
public class Person {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="person_id")
	private long personId;
	
	@Column(name="first_name")
	private String firstName;
	
	@Column(name="last_name")
	private String lastName;
	
	@Column(name="linkedin_url")
	private String linkedinUrl;
	
	private String whatsapp;
	
	private String mail;
	
	@ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinTable(name = "person_skill", joinColumns = @JoinColumn(name = "personId", referencedColumnName = "person_id"), inverseJoinColumns = @JoinColumn(name = "skillId", referencedColumnName = "skill_id"))
	private Set<Skill> skills = new HashSet<Skill>();
	
	public Person(long personId, String firstName, String lastName, String linkedinUrl, String whatsapp, String mail) {
		this.personId =  personId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.linkedinUrl = linkedinUrl;
		this.whatsapp = whatsapp;
		this.mail = mail;
	}
	
	public Person() {
		
	}
	
	
	public long getPersonId() {
		return personId;
	}
	public void setPersonId(long personId) {
		this.personId = personId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getLinkedinUrl() {
		return linkedinUrl;
	}
	public void setLinkedinUrl(String linkedinUrl) {
		this.linkedinUrl = linkedinUrl;
	}
	public String getWhatsapp() {
		return whatsapp;
	}
	public void setWhatsapp(String whatsapp) {
		this.whatsapp = whatsapp;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	
    public Set<Skill> getSkills() {
        return skills;
    }
    public void setSkills(Set<Skill> skills) {
        this.skills = skills;
    }
    
    @Override
	public String toString() {
		return "Person [id=" + getPersonId() + ", name=" + getFirstName() + " " + getLastName();
	}
}