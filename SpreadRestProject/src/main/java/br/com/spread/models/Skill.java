package br.com.spread.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;

/**
 * Class represents a person´s skill
 * Table = skill
 * @author VANESSA
 *
 */
@Entity
@NamedQuery(query = "select s from Skill s", name = "query_find_all_skill")
public class Skill {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="skill_id")
	public long skillId;
	
	@Column(name="skill_tag")
	public String skillTag;
	
	@Column(name="skill_description")
	public String skillDescription;
	
	@ManyToMany(mappedBy = "skills", fetch = FetchType.EAGER)
	private Set<Person> person = new HashSet<Person>();
	
	
	public Skill(long skillId, String skillTag, String skillDescription) {
		this.skillId =  skillId;
		this.skillTag = skillTag;
		this.skillDescription = skillDescription;
	}
	
	public Skill(long skillId) {
		this.skillId =  skillId;
	}
	
	public Skill() {
		
	}
	
	public long getSkillId() {
		return skillId;
	}
	public void setSkillId(long skillId) {
		this.skillId = skillId;
	}

	public String getSkillTag() {
		return skillTag;
	}
	public void setSkillTag(String skillTag) {
		this.skillTag = skillTag;
	}

	public String getSkillDescription() {
		return skillDescription;
	}
	public void setSkillDescription(String skillDescription) {
		this.skillDescription = skillDescription;
	}
	
    public void setPerson(Set<Person> person) {
        this.person = person;
    }
}