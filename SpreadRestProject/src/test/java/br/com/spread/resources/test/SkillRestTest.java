package br.com.spread.resources.test;

import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.spread.models.Skill;

@RunWith(SpringRunner.class)
public class SkillRestTest {

	final private String URL = "http://localhost:8080/spread/";

	final private String ADD_SKILL_TARGET = "/addSkill";
	final private String DELETE_SKILL_TARGET = "/deleteSkill/";
	final private String GET_SKILL_BY_ID_TARGET = "/skill/";
	final private String GET_ALL_SKILLS_TARGET = "/allSkills";

	@Test
	public void getAllSkills() {
		
		Client client = ClientBuilder.newClient();
		WebTarget base = client.target(URL);
		WebTarget people = base.path(GET_ALL_SKILLS_TARGET);
		List<Skill> list = people.request(MediaType.APPLICATION_JSON)
				.get(new GenericType<List<Skill>>() {});

		list.stream().forEach(skill -> 
			System.out.println(skill.getSkillId()+", "+ skill.getSkillTag()+", "+ skill.getSkillDescription()));
		
		client.close();
	}
	
	@Test
	public void addSkill() {
		
		Skill skill = new Skill(5, "Tag5", "Description5");
		
		new SkillRestTest().addSkill(skill);
	}
	
	private void addSkill(Skill skill) {

		Client client = ClientBuilder.newClient();
		WebTarget base = client.target(URL);
		WebTarget add = base.path(ADD_SKILL_TARGET);
		Response response = add.request(MediaType.APPLICATION_JSON)
				.post(Entity.json(skill));

		System.out.println("Response Http Status: "+ response.getStatus());
		System.out.println(response.getLocation());
		
		new SkillRestTest().getSkillById(skill.getSkillId());

		client.close();
	}
	
	public void getSkillById (long skillId) {
		
		Client client = ClientBuilder.newClient();
		WebTarget base = client.target(URL);
		WebTarget personById = base.path(GET_SKILL_BY_ID_TARGET.concat("{skillId}")).resolveTemplate("skillId", skillId);
		Skill skill = personById.request(MediaType.APPLICATION_JSON)
				.get(Skill.class);

		System.out.println(skill.getSkillId()+", "+ skill.getSkillTag()+", "+ skill.getSkillDescription());
		
		new SkillRestTest().deleteSkill(skillId);

		client.close();
	}
	
	public void deleteSkill (long skillId) {

		Client client = ClientBuilder.newClient();
		WebTarget base = client.target(URL);
		WebTarget deleteById = base.path(DELETE_SKILL_TARGET.concat("{skillId}")).resolveTemplate("skillId", skillId);
		Response response = deleteById.request(MediaType.APPLICATION_JSON)
				.delete();

		System.out.println("Response Http Status: "+ response.getStatus());
		if(response.getStatus() == 204) {
			System.out.println("Data deleted successfully.");
		}
		
		client.close();
	}
}
