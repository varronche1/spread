package br.com.spread.resources.test;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.spread.models.Person;
import br.com.spread.models.Skill;

@RunWith(SpringRunner.class)
public class PersonRestTest {

	final private String URL = "http://localhost:8080/spread/";

	final private String ADD_PERSON_TARGET = "/addPerson";
	final private String DELETE_PERSON_TARGET = "/deletePerson/";
	final private String GET_PERSON_BY_ID_TARGET = "/person/";
	final private String GET_PEOPLE_WITHOUT_SKILL_TARGET = "/justPeople";
	final private String GET_PEOPLE_AND_SKILLS_TARGET = "/peopleSkill";

	@Test
	public void getPeopleWithoutSkill() {
		Client client = ClientBuilder.newClient();
		WebTarget base = client.target(URL);
		WebTarget people = base.path(GET_PEOPLE_WITHOUT_SKILL_TARGET);
		List<Person> list = people.request(MediaType.APPLICATION_JSON)
				.get(new GenericType<List<Person>>() {});

		list.stream().forEach(person -> 
			System.out.println(person.getPersonId()+", "+ person.getFirstName()+", "+ person.getLastName()+
				", "+ person.getLinkedinUrl()+", "+ person.getWhatsapp()+", "+ person.getMail() +", "+ person.getSkills()));
		
		client.close();
	}
	
	@Test
	public void getPeopleAndSkill() {
		Client client = ClientBuilder.newClient();
		WebTarget base = client.target(URL);
		WebTarget people = base.path(GET_PEOPLE_AND_SKILLS_TARGET);
		List<Person> list = people.request(MediaType.APPLICATION_JSON)
				.get(new GenericType<List<Person>>() {});

		list.stream().forEach(person -> 
			System.out.println(person.getPersonId()+", "+ person.getFirstName()+", "+ person.getLastName()+
				", "+ person.getLinkedinUrl()+", "+ person.getWhatsapp()+", "+ person.getMail() +", "+ person.getSkills()));
		
		client.close();
	}
	
	@Test
	public void addPerson() {
		Set<Skill> skills = new HashSet<Skill>();
		
		Person person = new Person(5l, "Person5", "Person5", "linkedin5.com.br", "11944444444", "mail5@mail.com.br");
		Skill skill1 = new Skill(1, "Tag1", "Description1");
		Skill skill2 = new Skill(2, "Tag2", "Description2");
		skills.add(skill1);
		skills.add(skill2);
		person.setSkills(skills);
		
		new PersonRestTest().addPerson(person);
	}
	
	private void addPerson(Person person) {

		Client client = ClientBuilder.newClient();
		WebTarget base = client.target(URL);
		WebTarget add = base.path(ADD_PERSON_TARGET);
		Response response = add.request(MediaType.APPLICATION_JSON)
				.post(Entity.json(person));

		System.out.println("Response Http Status: "+ response.getStatus());
		System.out.println(response.getLocation());
		
		new PersonRestTest().getPersonById(person.getPersonId());

		client.close();
	}
	
	public void getPersonById (long personId) {
		
		Client client = ClientBuilder.newClient();
		WebTarget base = client.target(URL);
		WebTarget personById = base.path(GET_PERSON_BY_ID_TARGET.concat("{personId}")).resolveTemplate("personId", personId);
		Person person = personById.request(MediaType.APPLICATION_JSON)
				.get(Person.class);

		System.out.println(person.getPersonId()+", "+ person.getFirstName()+", "+ person.getLastName()+
				", "+ person.getLinkedinUrl()+", "+ person.getWhatsapp()+", "+ person.getMail());
		
		Set<Skill> skills = person.getSkills();
		Iterator<Skill> it = skills.iterator();
		while(it.hasNext()) {
			Skill s = it.next();
			System.out.println(s.getSkillId()+", "+ s.getSkillTag()+", "+ s.getSkillDescription());
		}
		
		new PersonRestTest().deletePerson(personId);

		client.close();
	}
	
	public void deletePerson (long personId) {

		Client client = ClientBuilder.newClient();
		WebTarget base = client.target(URL);
		WebTarget deleteById = base.path(DELETE_PERSON_TARGET.concat("{personId}")).resolveTemplate("personId", personId);
		Response response = deleteById.request(MediaType.APPLICATION_JSON)
				.delete();

		System.out.println("Response Http Status: "+ response.getStatus());
		if(response.getStatus() == 204) {
			System.out.println("Data deleted successfully.");
		}
		
		client.close();
	}
}
