# README #

As tabelas e os dados serão criados/inseridos a partir do arquivo que se encontra em resources/data.sql

A aplicação será iniciada através do comando: mvn spring-boot:run

Para executar os testes juntamente com a inicialização, executar o comando: mvn spring-boot:run test

Após inicialização da aplicação, o database H2 poderá ser acessado através de sua console: http://localhost:8080/console

Os serviços rest poderão ser acessados através das seguintes urls:

- Retornar todas as entidades "person" sem relacionamentos com outra entidade "skill"
http://localhost:8080/spread/justPeople

- Retornar as entidades "person" com seus relacionamentos com entidades "skill":
http://localhost:8080/spread/peopleSkill

- Entidade person com seus skills (apenas sendo obrigatório a informação do id de person)
http://localhost:8080/spread/person/1

Os testes, além de testarem os itens descritos acima, testará também o CRUD tanto da entidade person quanto de skill.